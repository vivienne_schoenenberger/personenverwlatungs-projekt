const URL = 'http://localhost:8080';
let entries = [];
let mode = 'create';
let currentEntry;

// API Requests
const createEntry = (entry) => {
    fetch(`${URL}/people`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(entry)
    }).then((result) => {
        result.json().then((entry) => {
            entries.push(entry);
            renderEntries();
        });
    });
};

const indexEntries = () => {
    fetch(`${URL}/people`, {
        method: 'GET'
    }).then((result) => {
        result.json().then((result) => {
            entries = result;
            renderEntries();
        });
    });
    renderEntries();
};

const deleteEntry = (id) => {
    fetch(`${URL}/people/${id}`, {
        method: 'DELETE'
    }).then((result) => {
        indexEntries();
    });
};

const updateEntry = (entry) => {
    fetch(`${URL}/people/${entry.id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(entry)
    }).then((result) => {
        result.json().then((entry) => {
            entries = entries.map((e) => e.id === entry.id ? entry : e);
            renderEntries();
        });
    });
}

// Rendering
const resetForm = () => {
    const entryForm = document.querySelector('#entryForm');
    entryForm.reset();
    mode = 'create';
    currentEntry = null;
}

const saveForm = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const entry = {};
    entry['firstname'] = formData.get('firstname');
    entry['lastname'] = formData.get('lastname');
    entry['email'] = formData.get('email');
    if (mode === 'create') {
        createEntry(entry);
    } else {
        entry.id = currentEntry.id;
        updateEntry(entry);
        mode = 'create';
    }
    resetForm();
}
const editEntry = (entry) => {
    mode = 'edit';
    currentEntry = entry;
    const entryForm = document.querySelector('#form');
    const firstname = entryForm.querySelector('[name="firstname"]');
    firstname.value = entry.firstname;
    const lastname = entryForm.querySelector('[name="lastname"]');
    lastname.value = entry.lastname;
    const email = entryForm.querySelector('[name="email"]');
    email.value = entry.email;
}
const createCell = (text) => {
    const cell = document.createElement('td');
    cell.innerText = text;
    return cell;
};
const createActions = (entry) => {
    const cell = document.createElement('td');

    const deleteButton = document.createElement('button');
    deleteButton.innerText = 'Delete';
    deleteButton.style=
    deleteButton.addEventListener('click', () => deleteEntry(entry.id));
    cell.appendChild(deleteButton);

    const editButton = document.createElement('button');
    editButton.innerText = 'Edit';
    editButton.addEventListener('click', () => editEntry(entry));
    cell.appendChild(editButton);

    return cell;
}
const renderEntries = () => {
    const display = document.querySelector('#peopleDisplay');
    display.innerHTML = '';
    entries.forEach((entry) => {
        const row = document.createElement('tr');
        row.appendChild(createCell(entry.id));
        row.appendChild(createCell(entry.firstname));
        row.appendChild(createCell(entry.lastname));
        row.appendChild(createCell(entry.email))
        row.appendChild(createActions(entry));
        display.appendChild(row);
    });
};
document.addEventListener('DOMContentLoaded', function(){
    const entryForm = document.querySelector('#form');
    entryForm.addEventListener('submit', saveForm);
    entryForm.addEventListener('reset', resetForm);
    indexEntries();
});