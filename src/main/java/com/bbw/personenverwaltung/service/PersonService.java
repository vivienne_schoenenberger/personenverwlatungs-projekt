package com.bbw.personenverwaltung.service;

import com.bbw.personenverwaltung.domain.Person;
import com.bbw.personenverwaltung.repository.PersonRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonService {

    // der Service braucht ein Repo um auf die DB zugang zu haben
    private PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }
    // eine Person erfassen:
    public Person createPerson (Person person){ return personRepository.save(person);}

    // alle Personen anzeigen
    public List<Person> findAll() {
        return personRepository.findAll();
    }

    public Optional<Person> searchPersonByID (long id){ return personRepository.findById(id);}

    // Person löschen
    public void deleteEntry(long id) { personRepository.deleteById(id); }

    // Personendaten aktualisieren
    public Person updatePerson(Person person) {
        return personRepository.save(person);
    }

    //patch
    //public Person patchPerson(Person person,long id){return personRepository.save(person, id)}

}


