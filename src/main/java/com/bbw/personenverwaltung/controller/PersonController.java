package com.bbw.personenverwaltung.controller;
import com.bbw.personenverwaltung.domain.Person;
import com.bbw.personenverwaltung.service.PersonService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/people")
public class PersonController {

    private PersonService personService;
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Person> getAllEntries() {
        return personService.findAll();
    }
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Optional<Person> getPersonById(@PathVariable long id) {
        return personService.searchPersonByID(id);
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Person createEntry(@RequestBody Person person) {
        System.out.println(person.getLastname());
        return personService.createPerson(person);
    }
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEntry(@PathVariable long id) {
        personService.deleteEntry(id);
    }
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Person updateEntry(@RequestBody Person person) {
        return personService.updatePerson(person);
    }

    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Person patchEntry(@RequestBody Person person,@PathVariable long id) {
        return personService.updatePerson(person);
    }


}
